import argparse, gzip, statistics
from Bio import SeqIO


def parseCatCalls(inPath, locHash, covHash, locCovHash):
	callReader=gzip.open(inPath, "rt")

	first=True
	lastID=None
	locID=None
	sampleList=[]
	coverages={}

	for line in callReader:
		if not line.startswith("#"):
			#print(line)
			first=False
			splitted=line.strip().split("\t")
			locID=splitted[0]
			curPos=int(splitted[1])
			ref=splitted[3]
			alt=splitted[4]


			if lastID!=locID:
				if lastID != None:
					#print(locID)
					for s in sampleList:				
						if s in coverages:
							mCov=statistics.mean(coverages[s])
							locCovHash[s][lastID]=mCov

							if mCov>0:
								#print(s, mCov, fragSize)
								if fragSize not in locHash[s]:
									locHash[s][fragSize]=1
								else:
									locHash[s][fragSize]+=1

								if fragSize not in covHash[s]:
									covHash[s][fragSize]=[mCov]
								else:
									covHash[s][fragSize].append(mCov)
						else:
							locCovHash[s][lastID]=0

				coverages={}
				fragSize=0
				lastID=locID			

			cov=0
			for i, sampleCov in enumerate(splitted[9:]):
				sID=sampleList[i]
				#print(i, sID)
				
				if alt == "." and sampleCov!=".": 
					cov=int(sampleCov)				
					if sID in coverages:
						coverages[sID].append(cov)
					else:
						coverages[sID]=[cov]
			fragSize=curPos

		#header
		elif not line.startswith("##"):
			splitted=line.strip().split("\t")
			for sample in splitted[9:]:
				#print(sample)
				sampleList.append(sample)
				covHash[sample]={}
				locHash[sample]={}
				locCovHash[sample]={}
			#print(sampleList)
			#coverages={}


	callReader.close()

	#LAST LOCUS
	for s in sampleList:				
		if s in coverages:
			mCov=statistics.mean(coverages[s])
			locCovHash[s][locID]=mCov
			if mCov>0:
				#print(s, mCov, fragSize)
				if fragSize not in locHash[s]:
					locHash[s][fragSize]=1
				else:
					locHash[s][fragSize]+=1

				if fragSize not in covHash[s]:
					covHash[s][fragSize]=[mCov]
				else:
					covHash[s][fragSize].append(mCov)
		else:
			locCovHash[s][locID]=0

	return sampleList

parser = argparse.ArgumentParser(description= "filters the stacks catalog.fa.gz for length, internal restriction sites and coverage")
parser.add_argument("--c", "-catalog", help="catalog.fa.gz of gstacks output")
parser.add_argument("--ca", "-calls", help="catalog.calls of gstacks output")
parser.add_argument("--min", "-minLen", help="minimum length for a locus to be selected", type=int)
parser.add_argument("--max", "-maxLen", help="maximum length for a locus to be selected", type=int)
parser.add_argument("--r1", "-rsite1", help="restriction site 1 to check for internal presence", type=str)
parser.add_argument("--r2", "-rsite2", help="restriction site 2 to check for internal presence", type=str)
parser.add_argument("--o", "-outfile", help="fasta output for filtered catalog")
args = parser.parse_args()



catReader=gzip.open(args.c, "rt")

writer=open(args.o, "w")

lHash={}
cHash={}
lcHash={}
sampleList=[]


sList=parseCatCalls(args.ca, lHash, cHash, lcHash)


for rec in SeqIO.parse(catReader, "fasta"):
	recLen=len(rec.seq)


	# 1) length filter
	if recLen < args.min or recLen > args.max:
		#print("Locus %s removed - length=%i out range!"%(rec.id, recLen))
		continue

	# 2) Restriction site filter
	enz1=args.r1.upper()
	enz2=args.r2.upper()
	if enz1 in rec.seq.upper() or enz2 in rec.seq.upper():
		#print("Locus %s removed - internal restriction site!"%(rec.id))
		continue


	# 3) coverage filter
	cov=True
	for s in sList:

		recCov=lcHash[s][rec.id]

		if recLen in cHash[s]:
			#only one value avoid error in sdev calc
			if len(cHash[s][recLen])==1:
				covMean=cHash[s][recLen]
				covSDEV=0
			else:
				covMean=statistics.mean(cHash[s][recLen])
				covSDEV=statistics.stdev(cHash[s][recLen])
		else:
			covSDEV=0
			covMean=0

		if recCov < covMean-(3*covSDEV) or recCov > covMean+(3*covSDEV):
			#print("Locus %s removed - coverage(%f) out of range!"%(rec.id, recCov))
			cov=False

	if cov==True:
		SeqIO.write(rec, writer, "fasta")
		#print(s, rec.id, recLen, lcHash[s][rec.id], covMean, covSDEV)
	#print()

catReader.close()
writer.close()