import argparse
from Bio import SeqIO

parser = argparse.ArgumentParser(description= "")
parser.add_argument("-fa", "--fasta", help="fasta file used for clustering")
parser.add_argument("-cl", "--clusters", help="output of Felix clusterig script")
parser.add_argument("-o", "--out", help="output fasta file")
args = parser.parse_args()

faReader=open(args.fasta)
clParser=open(args.clusters)
oWriter=open(args.out, "w")

snglH={}

for line in clParser:
	splitted=line.strip().split("\t")
	clID=splitted[0]

	clSize=int(splitted[1])
	if clSize==1:
		snglH[splitted[2]]=1

clParser.close()

for rec in SeqIO.parse(faReader, "fasta"):
	if rec.id in snglH:
		oWriter.write(">Stacks_%s\n"%(rec.id))
		oWriter.write("%s\n"%(rec.seq))

faReader.close()
oWriter.close()