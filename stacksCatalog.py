import argparse, gzip, statistics


def parseCatCalls(inPath, locHash, covHash, locCovHash):
	callReader=gzip.open(inPath, "rt")

	first=True
	lastID=None
	locID=None
	sampleList=[]
	coverages={}

	for line in callReader:
		if not line.startswith("#"):
			#print(line)
			first=False
			splitted=line.strip().split("\t")
			locID=splitted[0]
			curPos=int(splitted[1])
			ref=splitted[3]
			alt=splitted[4]


			if lastID!=locID:
				if lastID != None:
					#print(locID)
					for s in sampleList:				
						if s in coverages:
							mCov=statistics.mean(coverages[s])
							locCovHash[s][lastID]=mCov

							if mCov>0:
								#print(s, mCov, fragSize)
								if fragSize not in locHash[s]:
									locHash[s][fragSize]=1
								else:
									locHash[s][fragSize]+=1

								if fragSize not in covHash[s]:
									covHash[s][fragSize]=[mCov]
								else:
									covHash[s][fragSize].append(mCov)
						else:
							locCovHash[s][lastID]=0

				coverages={}
				fragSize=0
				lastID=locID			

			cov=0
			for i, sampleCov in enumerate(splitted[9:]):
				sID=sampleList[i]
				#print(i, sID)
				
				if alt == "." and sampleCov!=".": 
					cov=int(sampleCov)				
					if sID in coverages:
						coverages[sID].append(cov)
					else:
						coverages[sID]=[cov]
			fragSize=curPos

		#header
		elif not line.startswith("##"):
			splitted=line.strip().split("\t")
			for sample in splitted[9:]:
				#print(sample)
				sampleList.append(sample)
				covHash[sample]={}
				locHash[sample]={}
				locCovHash[sample]={}
			#print(sampleList)
			#coverages={}


	callReader.close()

	#LAST LOCUS
	for s in sampleList:				
		if s in coverages:
			mCov=statistics.mean(coverages[s])
			locCovHash[s][locID]=mCov
			if mCov>0:
				#print(s, mCov, fragSize)
				if fragSize not in locHash[s]:
					locHash[s][fragSize]=1
				else:
					locHash[s][fragSize]+=1

				if fragSize not in covHash[s]:
					covHash[s][fragSize]=[mCov]
				else:
					covHash[s][fragSize].append(mCov)
		else:
			locCovHash[s][locID]=0

	return sampleList
parser = argparse.ArgumentParser(description= "calculates the coverage per length in the stacks catalog")
parser.add_argument("--ca", "-calls", help="catalog.calls of gstacks output")
parser.add_argument("--o", "-outfile", help="output file")
args = parser.parse_args()


reader=gzip.open(args.ca, "rt")

writer=open(args.o, "w")


lHash={}
cHash={}
lcHash={}


sList=parseCatCalls(args.ca, lHash, cHash, lcHash)


for s in sList:
	#print(s)
	for l in  sorted(lHash[s].keys()):
		#print(s, l, locHash[s][l], statistics.mean(covHash[s][l]))
		if len(cHash[s][l])==1:
			stDEV=0
		else:
			stDEV=statistics.stdev(cHash[s][l])
		writer.write("%s\t%i\t%i\t%f\t%f\n"%(s, l, lHash[s][l], statistics.mean(cHash[s][l]), stDEV))
writer.close()