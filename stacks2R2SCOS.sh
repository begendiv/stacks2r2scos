
# output of gstacks containing catalog.calls and catalog.fa.gz
gstacksPATH="/home/max/Desktop/11_m3_stacks_KeepHighCov/Cc/M3/"
# path to the stacks2R2SCOs scripts
scriptPATH=/home/max/spree/projects/Turtles5species/07_stacks_comparison/final_scripts/stacks2R2SCOS/
# output directory for the stacks2R2SCOs coverage table and plot
covDIR="/home/max/Desktop/stacks2R2SCOS/covs/"

#### PART 1
mkdir -p $covDIR
python3 ${scriptPATH}/stacksCatalog.py --ca ${gstacksPATH}/catalog.calls --o $covDIR/coverages.tsv
Rscript ${scriptPATH}/plotStacksCatalog.R $covDIR/coverages.tsv $covDIR/coverages.svg


#### PART 2
# please adjust with correct size range to filter for and used resitriction enzymes
minSize=384
maxSize=448
rSite1="TTAA"
rSite2="GAATTC"

# output for filtered catalog
catOUT="/home/max/Desktop/stacks2R2SCOS/Cc_catalogFiltered.fa"
python3 ${scriptPATH}/filterCatalog.py --c ${gstacksPATH}/catalog.fa.gz --ca ${gstacksPATH}/catalog.calls --min $minSize --max $maxSize --r1 $rSite1 --r2 $rSite2 --o $catOUT

#### PART 3
Tintra=0.90 # Tintra 
target_cov=0.80
nthreads=8

# output directory for clustering output
clDIR="/home/max/Desktop/stacks2R2SCOS/clustering/"

mkdir -p $clDIR
vsearch --allpairs_global $catOUT --iddef 3 --alnout $clDIR/catalogFiltered.alnout --id $Tintra --qmask none --target_cov $target_cov --fasta_width 0 --threads $nthreads --userfields query+target+id3 --userout $clDIR/catalogFiltered.tabout
        
cd $clDIR
python3 ${scriptPATH}/clusterFromPairs.py $clDIR/catalogFiltered.tabout $catOUT 

# output for final filtered catalog
catOUT2="/home/max/Desktop/stacks2R2SCOS/Cc_catalogFiltered_SINGLETONS.fa"

python3 ${scriptPATH}/extractSINGELTONLociFA.py -fa $catOUT -cl $clDIR/catalogFiltered_clusters.tsv -o $catOUT2
